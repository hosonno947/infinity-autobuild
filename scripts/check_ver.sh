#!/bin/bash

set -x

curl -s --location --header "PRIVATE-TOKEN: $GL_TOKEN" \
"${CI_API_V4_URL}/projects/${CI_PROJECT_NAMESPACE}%2F${CI_PROJECT_NAME}/jobs/artifacts/main/raw/LAST_VER.txt?job=build"