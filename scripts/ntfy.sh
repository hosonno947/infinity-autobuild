#!/bin/bash

MESSAGE="$1"
APK_URL="$2"
TOPIC="$NTFY_TOPIC"
SERVER="${NTFY_SERVER:-ntfy.sh}"

curl -H "Title: New Infinity version available!" \
	-H "Tags: loudspeaker" \
	-H "Actions: view, Download, ${APK_URL}, clear=true" \
	-d "$MESSAGE
$APK_URL" \
	"$SERVER/$TOPIC"
