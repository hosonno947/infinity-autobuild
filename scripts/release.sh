#!/bin/bash
set -x

TOKEN="${API_TOKEN}"
TAG="$(git describe --tags --abbrev=0)"

getbody() {
	curl -s \
		-H "Accept: application/vnd.github+json" \
		https://api.github.com/repos/Docile-Alligator/Infinity-For-Reddit/releases/tags/$APP_VER | grep -Po '"body": "\K.+?(?=")'
}

curl --header 'Content-Type: application/json' --header "PRIVATE-TOKEN: $TOKEN" \
	--data '{
	"name": "'"${APP_VER}"'",
	"tag_name": "'"${TAG}"'",
	"ref": "'"${TAG}"'",
	"description": "'"$(getbody)"'",
	"assets": {
		"links": [{ 
		"name": "Infinity-'"${APP_VER}"'.apk", 
		"url": "https://gitlab.com/American_Jesus/infinity-for-reddit",
		"direct_asset_path": "/output/Infinity-'"${APP_VER}"'.apk",
		"link_type":"other" 
	}] } }' \
	--request POST "https://gitlab.com/api/v4/projects/American_Jesus%2Finfinity-for-reddit/releases"
